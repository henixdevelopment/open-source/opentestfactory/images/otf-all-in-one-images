DEFAULT_INTERPRETER_CUSTOM_RULES=/app/plugins/interpreter/interpreter.yaml

if [ -n "$INTERPRETER_CUSTOM_RULES" ]; then
  CONFIG_FILE_COMMAND="-Dcustom.parsers.file=${INTERPRETER_CUSTOM_RULES}";
else
  if [ -f "$DEFAULT_INTERPRETER_CUSTOM_RULES" ]; then
    CONFIG_FILE_COMMAND="-Dcustom.parsers.file=${DEFAULT_INTERPRETER_CUSTOM_RULES}";
  else
    CONFIG_FILE_COMMAND="";
  fi;
fi;

/bin/bash /app/plugins/launch_micronaut_service.sh otf-interpreter-${JAVA_PLUGINS_VERSION}.tar.gz otf-interpreter-${JAVA_PLUGINS_VERSION}/otf-interpreter-native-image ${CONFIG_FILE_COMMAND}
